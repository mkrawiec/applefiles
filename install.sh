#!/usr/bin/env bash

setup_sudo()
{
    # Ask for the administrator password upfront
    sudo -v

    # Keep-alive: update existing `sudo` time stamp until `.macos` has finished
    while true; do sudo -n true; sleep 60; kill -0 "$$" || exit; done 2>/dev/null &
}

run_scripts()
{
    for f in script/*.sh; do
        bash "$f" -H || break
    done
}

main()
{
    setup_sudo
    run_scripts
}

main $@
